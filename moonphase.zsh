#!/usr/bin/zsh

knownNewMoon=1516155420 # 2018-01-17 2:17 UTC was a new moon
synodicMonth=29.530588853
synodicMonthSecs=$(( synodicMonth * 24 * 60 * 60 ))
now=$(date +%s)
phaseIcons=(                             )

moonPhase=$(( (now - knownNewMoon) % synodicMonthSecs / synodicMonthSecs * 28 ))
moonPhaseRounded=$(( moonPhase + 0.5 + 1 )) # bash arrays start at 1
if [[ $moonPhaseRounded -ge 29 ]]; then
    moonPhaseRounded=$(( moonPhaseRounded - 28 ))
fi
echo ${phaseIcons[moonPhaseRounded]} # non-integer indices are always rounded down
