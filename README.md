Bar Utilities
=============

These are Bash scripts and Haskell programs that I use in my bar. The bash scripts are accessible as top-level `.sh` files; the Haskell binaries can be found in `dist/`.

Usage
-----
- `weather`
    - Uses DarkSky.net to fetch the weather. Requires you to have a developer secret key, stored in `$XDG_CONFIG_HOME/barutils/weather_appid`. Requires you to have your coordinates stored in `$XDG_CONFIG_HOME/barutils/weather_location`, in the form <latitude>,<longitude>.
        - If you don't recognize `$XDG_CONFIG_HOME`, it's probably `$HOME/.config`.
        - Currently only operates in Celsius. Fixing this is high on my TODO list.
        - Depends on [weather-icons](https://erikflowers.github.io/weather-icons/).
- `fuzzy`
    - A fuzzy clock. By default, uses 24-hour format. To use 12-hour format, it accepts the flag `--12`.
- `moonphase.zsh`
    - Displays the current phase of the moon.
    - Uses zsh's more friendly arithmetic.
    - Depends on [weather-icons](https://erikflowers.github.io/weather-icons/).
- `battery.sh`
    - Tiny bash script that adjusts its icon with how full the battery is, and whether it's charging.
    - Depends on [fontice](https://fontice.com).
