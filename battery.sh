battery="/org/freedesktop/UPower/devices/DisplayDevice"
batStatus="$(upower -i $battery)"
charging="$(echo "$batStatus" | grep state | sed 's/\s*state:\s*//')"
level="$(echo "$batStatus" | grep percentage | sed 's/\s*percentage:\s*//')"

# round percentage to an integer
level="$(echo "$level" | sed 's/%//')"
level="$(printf '%.0f' $level)"

fullAt=98

if [[ "$charging" = "charging" ]]; then
    prefix=
elif [[ "$level" -ge $fullAt ]]; then
    prefix=
elif [[ "$level" -ge 67 ]]; then
    prefix=
elif [[ "$level" -ge 33 ]]; then
    prefix=
else
    prefix=
fi

echo "$prefix $level%"
