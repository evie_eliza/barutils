{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}

module Weather where

import Control.Lens
import Control.Monad ((>=>), (<=<))
import Data.Aeson
import Data.Aeson.Types
import Data.List (intercalate)
import Data.Scientific
import Network.Wreq

import Args (toField, Settings(..), Temperature(..))

processWeather :: Settings -> Value -> (Temperature, String)
processWeather Settings{unit, apparent} weather =
  let parseWeather x = parse (withObject "Expected object as response" x) weather
      temp = Temperature unit $ case parseWeather ((.: "currently") >=> (.: toField apparent)) of
                                  Error e -> scientific 1337 0-- maybe I should actually throw an error here. whatever.
                                  Success t -> t
      icon = case parseWeather ((.: "currently") >=> (.: "icon")) of
               Error e -> ""
               Success i -> i
   in (temp, icon)

getWeather :: Settings -> IO (Temperature, String)
getWeather settings@Settings{unit, baseUrl, location, appid} =

  let apiUrl  = intercalate "/" [baseUrl, appid, location]
      opts    = defaults & params .~ [ ("exclude", "minutely,hourly,daily,alerts,flags")
                                     , ("units", toField unit)
                                     ]
      weather = (^. responseBody) <$> (asValue =<< getWith opts apiUrl)
   in processWeather settings <$> weather
