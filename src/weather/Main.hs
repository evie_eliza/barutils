import Prelude hiding (lookup)

import Data.HashMap.Strict (HashMap(..), lookup, lookupDefault, insert, empty)
import Data.Scientific
import System.Environment

import Weather
import Args

icons :: HashMap String Char
icons = foldl (flip $ uncurry insert) empty
  [ ("clear-day", '\xf00d')
  , ("clear-night", '\xf031')
  , ("rain", '\xf019')
  , ("snow", '\xf01b')
  , ("sleet", '\xf0b5')
  , ("wind", '\xf050')
  , ("fog", '\xf014')
  , ("cloudy", '\xf041')
  , ("partly-cloudy-day", '\xf002')
  , ("partly-cloudy-night", '\xf083')
  , ("default", '\xf055')
  ]

getIcon :: String -> Char
getIcon k = lookupDefault def k icons
  where def = case lookup "default" icons of
                Just x -> x
                Nothing -> error "Someone fucked up. Check the default icon."

main = do
  (temp, icon) <- getWeather =<< getSettings
  putStrLn $ getIcon icon : ' ' : show temp
