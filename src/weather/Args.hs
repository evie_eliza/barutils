{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Args where

import Data.Scientific
import Data.Text (unpack, strip, Text)
import qualified Data.Text.IO as T
import System.Environment
import Options.Applicative

class Option a where
  toField :: a -> Text

data Unit = Celsius | Fahrenheit deriving (Show, Eq)

instance Option Unit where
  toField Celsius    = "si"
  toField Fahrenheit = "imperial"

newtype Apparent = Apparent Bool deriving (Show, Eq)

instance Option Apparent where
  toField (Apparent True)  = "apparentTemperature"
  toField (Apparent False) = "temperature"

data Settings = Settings
  { unit     :: Unit
  , apparent :: Apparent
  , location :: String
  , baseUrl  :: String
  , appid    :: String
  } deriving (Show, Eq)

data Temperature = Temperature
  { tUnit :: Unit
  , tValue :: Scientific
  } deriving (Eq)

sci2str :: Scientific -> String
sci2str = positiveZero . formatScientific Fixed (Just 0) -- round to a whole number
  where positiveZero "-0" = "0"
        positiveZero x = x

instance Show Temperature where
  show (Temperature Celsius v)    = sci2str v ++ " °C"
  show (Temperature Fahrenheit v) = sci2str v ++ " °F"

readConfigFile :: FilePath -> IO String
readConfigFile p = unpack . strip <$> (T.readFile =<< (++ "/barutils/" ++ p) <$> getConfigHome)
  where getConfigHome = lookupEnv "XDG_CONFIG_HOME"
                          >>= \case
                                  Just x  -> return x
                                  Nothing -> (++ "/.config") <$> getEnv "HOME"

apiBaseUrl :: String
apiBaseUrl = "https://api.darksky.net/forecast"

getSettings :: IO Settings
getSettings = do
  defAppid <- readConfigFile "weather_appid"
  defLocation <- readConfigFile "weather_location"
  let settings = pure Settings
                  <*> pure Celsius  {- The logic for this behavior is all in
                                       place, but using it is forbidden,
                                       for reasons that I know and you don't, stranger.
                                     -}
                  <*> flag (Apparent False) (Apparent True)
                        ( long "apparent"
                       <> short 'a'
                        )
                  <*> argument str
                        ( metavar "<lat>,<lon>"
                       <> value defLocation
                        )
                  <*> pure apiBaseUrl
                  <*> strOption
                        ( long "appid"
                       <> short 'i'
                       <> value defAppid
                       <> metavar "APPID"
                        )
  -- using forwardOptions so that negative latitudes are read as arguments, e.g. -3.14,2.71
  execParser $ info (settings <**> helper) (fullDesc <> forwardOptions)
