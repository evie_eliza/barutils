import Data.List
import Data.Time.LocalTime
import System.Environment

cardinalName :: Int -> String
cardinalName 0  = ""
cardinalName 1  = "one"
cardinalName 2  = "two"
cardinalName 3  = "three"
cardinalName 4  = "four"
cardinalName 5  = "five"
cardinalName 6  = "six"
cardinalName 7  = "seven"
cardinalName 8  = "eight"
cardinalName 9  = "nine"
cardinalName 10 = "ten"
cardinalName 11 = "eleven"
cardinalName 12 = "twelve"
cardinalName 13 = "thirteen"
cardinalName 15 = "fifteen"
cardinalName 18 = "eighteen"
cardinalName x = tennify tens $ cardinalName ones
  where tens = x `div` 10
        ones = x `mod` 10

ordinalName :: Int -> String
ordinalName 0  = ""
ordinalName 1  = "first"
ordinalName 2  = "second"
ordinalName 3  = "third"
ordinalName 4  = "fourth"
ordinalName 5  = "fifth"
ordinalName 6  = "sixth"
ordinalName 7  = "seventh"
ordinalName 8  = "eighth"
ordinalName 9  = "ninth"
ordinalName 10 = "tenth"
ordinalName 11 = "eleventh"
ordinalName 12 = "twelfth"
ordinalName 13 = "thirteenth"
ordinalName 15 = "fifteenth"
ordinalName x
  | x < 20 = cardinalName x ++ "th" -- "teenth"
  | ones > 0 = tennify tens $ ordinalName ones -- "twenty-first"
  | otherwise = init (cardinalName x) ++ "ieth" -- "twentieth"
  where tens = x `div` 10
        ones = x `mod` 10

tennify :: Int -> String -> String
tennify tens =
  case tens of
    0 -> id
    1 -> (++ "teen")
    2 -> prefix "twenty"
    3 -> prefix "thirty"
    4 -> prefix "forty"
    5 -> prefix "fifty"
    6 -> prefix "sixty"
    7 -> prefix "seventy"
    8 -> prefix "eighty"
    9 -> prefix "ninety"
  where prefix x "" = x
        prefix x y = x ++ "-" ++ y

roundTime :: (Int, Int) -> (Int, Int)
roundTime (hour', min') = (hour, min)
  where roundedMin = round (min' // 5) * 5 -- min rounded to a multiple of 5
        min = case roundedMin of
                60 -> 0
                x  -> x
        hour = case roundedMin of
                60 -> (hour' + 1) `mod` 24
                _  -> hour'
        (//) = (/) . fromIntegral

data Format = Standard | Dramatic deriving (Eq, Show)

fuzz :: Format -> Int -> Int -> String
fuzz Standard hour' min'
  | min == 0 && hour == 0  = hourify hour
  | min == 0 && hour == 12 = hourify hour
  | min == 0  = hourify hour ++ " o'clock"
  | min == 15 = "quarter past " ++ hourify hour
  | min == 30 = "half past " ++ hourify hour
  | min == 45 = "quarter to " ++ hourify (succ hour)
  | min <= 30 = cardinalName min ++ " past " ++ hourify hour
  | otherwise = cardinalName (60 - min) ++ " to " ++ hourify (succ hour)
  where (hour, min) = roundTime (hour', min')
        succ = (`mod` 24) . (1+)
        hourify 0 = "midnight"
        hourify 12 = "noon"
        hourify x = cardinalName x

fuzz Dramatic hour' min'
  | min == 0 && hour == 0  = hourify hour
  | min == 0 && hour == 12 = hourify hour
  | min == 0  = hourify hour
  | min == 15 = "a quarter-turn of " ++ hourify hour
  | min == 30 = "a half-turn of " ++ hourify hour
  | min == 45 = "a quarter-turn before " ++ hourify (succ hour)
  | min <= 30 = cardinalName min ++ " ticks of " ++ hourify hour
  | otherwise = cardinalName (60 - min) ++ " ticks before " ++ hourify (succ hour)
  where (hour, min) = roundTime (hour', min')
        succ = (`mod` 24) . (1+)
        hourify 0 = "midnight"
        hourify 12 = "noontide"
        hourify x = "the " ++ ordinalName x ++ " hour"

main = do
  TimeOfDay hour' min _ <- localTimeOfDay . zonedTimeToLocalTime <$> getZonedTime
  args <- getArgs

  let twelveHour = "--12" `elem` args || "--twelve" `elem` args
  let drama = if "--dramatic" `elem` args
                    then Dramatic
                    else Standard

  let hour = if twelveHour && hour' > 12 && (hour' < 23 || min <= 30)
                then hour' - 12
                else hour'
  putStrLn $ fuzz drama hour min
